## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for GitLab. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for GitLab.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the GitLab. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getV4ApplicationSettings(callback)</td>
    <td style="padding:15px">Get the current application settings</td>
    <td style="padding:15px">{base_path}/{version}/application/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ApplicationSettings(body, callback)</td>
    <td style="padding:15px">Modify application settings</td>
    <td style="padding:15px">{base_path}/{version}/application/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4DeployKeys(callback)</td>
    <td style="padding:15px">Get a list of all deploy keys across all projects</td>
    <td style="padding:15px">{base_path}/{version}/deploy_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Groups(statistics, allAvailable, search, orderBy = 'name', sort = 'asc', skipGroups, withCustomAttributes, owned, minAccessLevel, callback)</td>
    <td style="padding:15px">Get a groups list</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Groups(body, callback)</td>
    <td style="padding:15px">Create a group. Available only for users who can create groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsId(id, callback)</td>
    <td style="padding:15px">Get a single group, with containing projects.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsId(id, body, callback)</td>
    <td style="padding:15px">Update a group. Available only for users who can administrate groups.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsId(id, callback)</td>
    <td style="padding:15px">Remove a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdAccessRequests(id, page, perPage, callback)</td>
    <td style="padding:15px">Gets a list of access requests for a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdAccessRequests(id, callback)</td>
    <td style="padding:15px">Requests access for the authenticated user to a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdAccessRequestsUserId(id, userId, callback)</td>
    <td style="padding:15px">Denies an access request for the given user.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/access_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdAccessRequestsUserIdApprove(id, userId, body, callback)</td>
    <td style="padding:15px">Approves an access request for the given user.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/access_requests/{pathv2}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdIssues(id, state = 'opened', labels, milestone, orderBy = 'created_at', sort = 'asc', page, perPage, callback)</td>
    <td style="padding:15px">Get a list of group issues</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMembers(id, query, page, perPage, callback)</td>
    <td style="padding:15px">Gets a list of group or project members viewable by the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdMembers(id, body, callback)</td>
    <td style="padding:15px">Adds a member to a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMembersUserId(id, userId, callback)</td>
    <td style="padding:15px">Gets a member of a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdMembersUserId(id, userId, body, callback)</td>
    <td style="padding:15px">Updates a member of a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdMembersUserId(id, userId, callback)</td>
    <td style="padding:15px">Removes a user from a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdNotificationSettings(id, callback)</td>
    <td style="padding:15px">Get group level notification level settings, defaults to Global</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdNotificationSettings(id, body, callback)</td>
    <td style="padding:15px">Update group level notification level settings, defaults to Global</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdProjectsWithOptions(id, queryData, callback)</td>
    <td style="padding:15px">Get a list of projects in this group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdProjectsProjectId(id, projectId, callback)</td>
    <td style="padding:15px">Transfer a project to the group namespace. Available only for admin.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdCustomAttributes(id, callback)</td>
    <td style="padding:15px">Get all custom attributes on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/custom_attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdCustomAttributesKey(key, id, callback)</td>
    <td style="padding:15px">Get a single custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdCustomAttributes(key, id, body, callback)</td>
    <td style="padding:15px">Set a custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdCustomAttributes(id, key, callback)</td>
    <td style="padding:15px">Delete a custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBadges(id, callback)</td>
    <td style="padding:15px">Gets a list of a group’s badges.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdBadges(id, body, callback)</td>
    <td style="padding:15px">Adds a badge to a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBadgesBadgeId(id, badgeId, callback)</td>
    <td style="padding:15px">Gets a badge of a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdBadgesBadgedId(id, badgeId, body, callback)</td>
    <td style="padding:15px">Updates a badge of a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdBadgesBadgesId(id, badgeId, callback)</td>
    <td style="padding:15px">Removes a badge from a group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBadgesRender(id, imageUrl, linkUrl, callback)</td>
    <td style="padding:15px">Returns how the link_url and image_url final URLs would be after resolving the placeholder interpolation.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/badges/render?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBoards(id, callback)</td>
    <td style="padding:15px">Lists Issue Boards in the given group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdBoards(name, id, callback)</td>
    <td style="padding:15px">Creates a board.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBoardsBoardId(boardId, id, callback)</td>
    <td style="padding:15px">Gets a single board.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdBoardsBoardId(boardId, name, assigneeId, milestoneId, weight, id, callback)</td>
    <td style="padding:15px">Updates a board.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdBoardsBoardId(id, boardId, callback)</td>
    <td style="padding:15px">Deletes a board.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBoardsBoardIdLists(boardId, id, callback)</td>
    <td style="padding:15px">Get a list of the board’s lists.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdBoardsBoardIdLists(labelId, id, boardId, callback)</td>
    <td style="padding:15px">Creates a new Issue Board list.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdBoardsBoardIdListsListId(id, boardId, listId, callback)</td>
    <td style="padding:15px">Get a single board list.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdBoardsBoardsIdListsListId(position, id, boardId, listId, callback)</td>
    <td style="padding:15px">Updates an existing Issue Board list.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdBoardsBoardIdListsListId(id, boardId, listId, callback)</td>
    <td style="padding:15px">Delete a board list</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdLabels(id, callback)</td>
    <td style="padding:15px">The ID or URL-encoded path of the group owned by the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdLabels(id, body, callback)</td>
    <td style="padding:15px">Updates an existing group label.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdLabels(id, body, callback)</td>
    <td style="padding:15px">Create a new group label for a given group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdLabels(name, id, callback)</td>
    <td style="padding:15px">Deletes a group label with a given name.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdLabelsLabelIdSubscribe(id, labelId, callback)</td>
    <td style="padding:15px">Subscribes the authenticated user to a group label to receive notifications.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels/{pathv2}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdLabelsLabelIdUnsubscribe(id, labelId, callback)</td>
    <td style="padding:15px">Unsubscribes the authenticated user from a group label to not receive notifications from it.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/labels/{pathv2}/unsubscribe/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdVariables(id, callback)</td>
    <td style="padding:15px">Get list of a group’s variables.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdVariables(id, key, value, callback)</td>
    <td style="padding:15px">Create a new variable.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdVariablesKey(id, key, value, callback)</td>
    <td style="padding:15px">Update a group’s variable.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdVariableKey(id, key, callback)</td>
    <td style="padding:15px">Remove a group’s variable.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMilestones(id, iids, search, state, title, callback)</td>
    <td style="padding:15px">Returns a list of group milestones.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdMilestones(id, body, callback)</td>
    <td style="padding:15px">Creates a new group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMilestonesMilestonesId(id, milestonesId, callback)</td>
    <td style="padding:15px">Gets a single group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdMilestonesMilestonesId(id, milestonesId, body, callback)</td>
    <td style="padding:15px">Updates an existing group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdMilestonesMilestonesId(id, milestonesId, callback)</td>
    <td style="padding:15px">Delete a group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMilestonesMilestoneIdMergeRequests(milestonesId, id, callback)</td>
    <td style="padding:15px">Gets all merge requests assigned to a single group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}/merge_request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMilestonesMilestoneIdIssues(id, milestonesId, callback)</td>
    <td style="padding:15px">Gets all issues assigned to a single group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdMilestonesMilestoneIdBurndownEvents(id, milestonesId, callback)</td>
    <td style="padding:15px">Gets all merge requests assigned to a single group milestone.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/milestones/{pathv2}/burndown_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdEpicsEpicsIdResourceLabelEvents(id, epicsId, callback)</td>
    <td style="padding:15px">Gets a list of all label events for a single epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/resource_label_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId(id, epicsId, resourceLabelEventId, callback)</td>
    <td style="padding:15px">Returns a single label event for a specific group epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/resource_label_events/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdSearch(scope, search, id, callback)</td>
    <td style="padding:15px">Search within the specified group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdEpicsIdDiscussions(id, epicsId, callback)</td>
    <td style="padding:15px">Gets a list of all discussions for a single epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdEpicsIdDiscussions(id, epicsId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Creates a new discussion to a single group epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdApproveMergeRequest(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Post a comment to a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4GroupsIdEpicsIdDiscussionsId(id, epicsId, discussionId, callback)</td>
    <td style="padding:15px">Returns a single discussion for a specific group epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4GroupsIdEpicsIdDiscussionsIdNotes(id, epicsId, discussionId, body, callback)</td>
    <td style="padding:15px">Adds a new note to the discussion.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions/{pathv3}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4GroupsIdEpicsIdDiscussionsIdNoteId(id, epicsId, discussionId, noteId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Modify existing discussion note of an epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4GroupsIdEpicsIdDiscussionsIdNoteId(id, epicsId, discussionId, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing discussion note of an epic.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/epics/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Hooks(callback)</td>
    <td style="padding:15px">Get the list of system hooks</td>
    <td style="padding:15px">{base_path}/{version}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Hooks(body, callback)</td>
    <td style="padding:15px">Create a new system hook</td>
    <td style="padding:15px">{base_path}/{version}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4HooksId(id, callback)</td>
    <td style="padding:15px">Test a hook</td>
    <td style="padding:15px">{base_path}/{version}/hooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4HooksId(id, callback)</td>
    <td style="padding:15px">Delete a hook</td>
    <td style="padding:15px">{base_path}/{version}/hooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Issues(state = 'opened', labels, milestone, orderBy = 'created_at', sort = 'asc', page, perPage, callback)</td>
    <td style="padding:15px">Get currently authenticated user's issues</td>
    <td style="padding:15px">{base_path}/{version}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4KeysId(id, callback)</td>
    <td style="padding:15px">Get single ssh key by id. Only available to admin users</td>
    <td style="padding:15px">{base_path}/{version}/keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Namespaces(callback)</td>
    <td style="padding:15px">Get a namespaces list</td>
    <td style="padding:15px">{base_path}/{version}/namespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NotificationSettings(callback)</td>
    <td style="padding:15px">Get global notification level settings and email, defaults to Participate</td>
    <td style="padding:15px">{base_path}/{version}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4NotificationSettings(body, callback)</td>
    <td style="padding:15px">Update global notification level settings and email, defaults to Participate</td>
    <td style="padding:15px">{base_path}/{version}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Projects(orderBy = 'id', sort = 'asc', archived, visibility = 'public', search, simple, owned, starred, membership, statistics, withCustomAttributes, withMergeRequestsEnabled, withProgrammingLanguage, wikiChecksumFailed, repositoryChecksumFailed, minAccessLevel, withIssuesEnabled, callback)</td>
    <td style="padding:15px">Get a projects list for authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Projects(body, callback)</td>
    <td style="padding:15px">Create new project</td>
    <td style="padding:15px">{base_path}/{version}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsUserUserId(userId, body, callback)</td>
    <td style="padding:15px">Create new project for a specified user. Only available to admin users.</td>
    <td style="padding:15px">{base_path}/{version}/projects/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsId(id, callback)</td>
    <td style="padding:15px">Get a single project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approveV4Project(id, body, callback)</td>
    <td style="padding:15px">Approve a single project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/approvals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsId(id, body, callback)</td>
    <td style="padding:15px">Update an existing project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsId(id, callback)</td>
    <td style="padding:15px">Remove a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdAccessRequests(id, page, perPage, callback)</td>
    <td style="padding:15px">Gets a list of access requests for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdAccessRequests(id, callback)</td>
    <td style="padding:15px">Requests access for the authenticated user to a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/access_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdAccessRequestsUserId(id, userId, callback)</td>
    <td style="padding:15px">Denies an access request for the given user.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/access_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdAccessRequestsUserIdApprove(id, userId, body, callback)</td>
    <td style="padding:15px">Approves an access request for the given user.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/access_requests/{pathv2}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdArchive(id, callback)</td>
    <td style="padding:15px">Archive a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBoards(id, callback)</td>
    <td style="padding:15px">Get all project boards</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBoardsBoardIdLists(id, boardId, callback)</td>
    <td style="padding:15px">Get the lists of a project board</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards/{pathv2}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdBoardsBoardIdLists(id, boardId, body, callback)</td>
    <td style="padding:15px">Create a new board list</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards/{pathv2}/lists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBoardsBoardIdListsListId(id, boardId, listId, callback)</td>
    <td style="padding:15px">Get a list of a project board</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdBoardsBoardIdListsListId(id, boardId, listId, body, callback)</td>
    <td style="padding:15px">Moves a board list to a new position</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdBoardsBoardIdListsListId(id, boardId, listId, callback)</td>
    <td style="padding:15px">Delete a board list</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/boards/{pathv2}/lists/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdDeployKeys(id, callback)</td>
    <td style="padding:15px">Get a specific project's deploy keys</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deploy_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdDeployKeys(id, body, callback)</td>
    <td style="padding:15px">Add new deploy key to currently authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deploy_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdDeployKeysKeyId(id, keyId, callback)</td>
    <td style="padding:15px">Get single deploy key</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deploy_keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdDeployKeysKeyId(id, keyId, callback)</td>
    <td style="padding:15px">Delete deploy key for a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deploy_keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdDeployKeysKeyIdEnable(id, keyId, callback)</td>
    <td style="padding:15px">Enable a deploy key for a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deploy_keys/{pathv2}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdDeployments(id, page, perPage, callback)</td>
    <td style="padding:15px">Get all deployments of the project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdDeploymentsDeploymentId(id, deploymentId, callback)</td>
    <td style="padding:15px">Gets a specific deployment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdEnvironments(id, page, perPage, callback)</td>
    <td style="padding:15px">Get all environments of the project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdEnvironments(id, body, callback)</td>
    <td style="padding:15px">Creates a new environment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsEnvironmentsEnvironmentId(id, environmentId, callback)</td>
    <td style="padding:15px">Get a specific environment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdEnvironmentsEnvironmentId(id, environmentId, body, callback)</td>
    <td style="padding:15px">Updates an existing environment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdEnvironmentsEnvironmentId(id, environmentId, callback)</td>
    <td style="padding:15px">Deletes an existing environment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdEvents(id, page, perPage, callback)</td>
    <td style="padding:15px">Get events for a single project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdFork(id, body, callback)</td>
    <td style="padding:15px">Forks a project into the user namespace of the authenticated user or the one provided.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/fork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdFork(id, callback)</td>
    <td style="padding:15px">Remove a forked_from relationship</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/fork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdForkForkedFromId(id, forkedFromId, callback)</td>
    <td style="padding:15px">Mark this project as forked from another</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/fork/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdHooks(id, page, perPage, callback)</td>
    <td style="padding:15px">Get project hooks</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdHooks(id, body, callback)</td>
    <td style="padding:15px">Add hook to project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdHooksHookId(id, hookId, callback)</td>
    <td style="padding:15px">Get a project hook</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdHooksHookId(id, hookId, body, callback)</td>
    <td style="padding:15px">Update an existing project hook</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdHooksHookId(id, hookId, callback)</td>
    <td style="padding:15px">Removes a hook from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssues(id, state = 'opened', iids, labels, milestone, orderBy = 'created_at', sort = 'asc', page, perPage, callback)</td>
    <td style="padding:15px">Get a list of project issues</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssues(id, body, callback)</td>
    <td style="padding:15px">Create a new project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdLabels(id, callback)</td>
    <td style="padding:15px">Get all labels of the project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdLabels(id, body, callback)</td>
    <td style="padding:15px">Update an existing label. At least one optional parameter is required.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdLabels(id, body, callback)</td>
    <td style="padding:15px">Create a new label</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdLabels(id, name, callback)</td>
    <td style="padding:15px">Delete an existing label</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMembers(id, query, page, perPage, callback)</td>
    <td style="padding:15px">Gets a list of group or project members viewable by the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMembers(id, body, callback)</td>
    <td style="padding:15px">Adds a member to a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMembersUserId(id, userId, callback)</td>
    <td style="padding:15px">Gets a member of a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMembersUserId(id, userId, body, callback)</td>
    <td style="padding:15px">Updates a member of a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMembersUserId(id, userId, callback)</td>
    <td style="padding:15px">Removes a user from a group or project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequests(id, state = 'opened', orderBy = 'created_at', sort = 'asc', page, perPage, callback)</td>
    <td style="padding:15px">List merge requests</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequests(id, body, callback)</td>
    <td style="padding:15px">Create a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMilestones(id, state = 'active', page, perPage, callback)</td>
    <td style="padding:15px">Get a list of project milestones</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMilestones(id, body, callback)</td>
    <td style="padding:15px">Create a new project milestone</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMilestonesMilestoneId(id, milestoneId, callback)</td>
    <td style="padding:15px">Get a single project milestone</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMilestonesMilestoneId(id, milestoneId, body, callback)</td>
    <td style="padding:15px">Update an existing project milestone</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectIdMilestonesId(id, milestoneId, callback)</td>
    <td style="padding:15px">Delete a project milestone.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMilestonesMilestoneIdIssues(id, milestoneId, callback)</td>
    <td style="padding:15px">Get all issues for a single project milestone</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/milestones/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdNotificationSettings(id, callback)</td>
    <td style="padding:15px">Get project level notification level settings, defaults to Global</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdNotificationSettings(id, body, callback)</td>
    <td style="padding:15px">Update project level notification level settings, defaults to Global</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/notification_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPipeline(id, body, callback)</td>
    <td style="padding:15px">Create a new pipeline</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPipelines(id, page, perPage, scope = 'running', callback)</td>
    <td style="padding:15px">Get all Pipelines of the project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipelines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPipelinesPipelineId(id, pipelineId, callback)</td>
    <td style="padding:15px">Gets a specific pipeline for the project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipelines/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPipelinesPipelineIdCancel(id, pipelineId, callback)</td>
    <td style="padding:15px">Cancel all builds in the pipeline</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipelines/{pathv2}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPipelinesPipelineIdRetry(id, pipelineId, callback)</td>
    <td style="padding:15px">Retry failed builds in the pipeline</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipelines/{pathv2}/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryArchive(id, sha, format, callback)</td>
    <td style="padding:15px">Get an archive of the repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryBlobsSha(id, sha, filepath, callback)</td>
    <td style="padding:15px">Get a raw file contents</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/blobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryBranches(id, callback)</td>
    <td style="padding:15px">Get a project repository branches</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryBranches(id, body, callback)</td>
    <td style="padding:15px">Create branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryBranchesBranch(id, branch, callback)</td>
    <td style="padding:15px">Get a single branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRepositoryBranchesBranch(id, branch, callback)</td>
    <td style="padding:15px">Delete a branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdRepositoryBranchesBranchProtect(id, branch, body, callback)</td>
    <td style="padding:15px">Protect a single branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches/{pathv2}/protect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdRepositoryBranchesBranchUnprotect(id, branch, callback)</td>
    <td style="padding:15px">Unprotect a single branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/branches/{pathv2}/unprotect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommits(id, refName, since, until, page, perPage, pathParam, callback)</td>
    <td style="padding:15px">Get a project repository commits</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryCommits(id, body, callback)</td>
    <td style="padding:15px">Commit multiple file changes as one commit</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommitsSha(id, sha, callback)</td>
    <td style="padding:15px">Get a specific commit of a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryCommitsShaCherryPick(id, sha, body, callback)</td>
    <td style="padding:15px">Cherry pick commit into a branch</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/cherry_pick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommitsShaComments(id, page, perPage, sha, callback)</td>
    <td style="padding:15px">Get a commit's comments</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryCommitsShaComments(id, sha, body, callback)</td>
    <td style="padding:15px">Post comment to commit</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommitsShaDiff(id, sha, callback)</td>
    <td style="padding:15px">Get the diff for a specific commit of a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommitsShaStatuses(id, sha, ref, stage, name, all, page, perPage, callback)</td>
    <td style="padding:15px">Get a commit's statuses</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCompare(id, from, to, callback)</td>
    <td style="padding:15px">Compare two branches, tags, or commits</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/compare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryContributors(id, callback)</td>
    <td style="padding:15px">Get repository contributors</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/contributors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryRawBlobsSha(id, sha, callback)</td>
    <td style="padding:15px">Get a raw blob contents by blob sha</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/raw_blobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryTags(id, callback)</td>
    <td style="padding:15px">Get a project repository tags</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryTags(id, body, callback)</td>
    <td style="padding:15px">Create a new repository tag</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryTagsTagName(id, tagName, callback)</td>
    <td style="padding:15px">Get a single repository tag</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRepositoryTagsTagName(id, tagName, callback)</td>
    <td style="padding:15px">Delete a repository tag</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdRepositoryTagsTagNameRelease(id, tagName, body, callback)</td>
    <td style="padding:15px">Update a tag's release note</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags/{pathv2}/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryTagsTagNameRelease(id, tagName, body, callback)</td>
    <td style="padding:15px">Add a release note to a tag</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tags/{pathv2}/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryTreeWithOptions(id, queryData, callback)</td>
    <td style="padding:15px">Get a project repository tree</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRunners(id, scope = 'active', status, tagList, callback)</td>
    <td style="padding:15px">Get runners available for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRunners(id, body, callback)</td>
    <td style="padding:15px">Enable a runner for a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRunnersRunnerId(id, runnerId, callback)</td>
    <td style="padding:15px">Disable project's runner</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/runners/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesAsana(id, callback)</td>
    <td style="padding:15px">Get Asana service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/asana?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesAsana(id, body, callback)</td>
    <td style="padding:15px">Set asana service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/asana?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesAsana(id, callback)</td>
    <td style="padding:15px">Delete Asana service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/asana?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesAssembla(id, callback)</td>
    <td style="padding:15px">Get Assembla service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/assembla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesAssembla(id, body, callback)</td>
    <td style="padding:15px">Set assembla service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/assembla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesAssembla(id, callback)</td>
    <td style="padding:15px">Get Assembla service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/assembla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesBamboo(id, callback)</td>
    <td style="padding:15px">Get Atlassian Bamboo CI service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bamboo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesBamboo(id, body, callback)</td>
    <td style="padding:15px">Set bamboo service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bamboo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesBamboo(id, callback)</td>
    <td style="padding:15px">Delete Atlassian Bamboo CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bamboo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesBugzilla(id, callback)</td>
    <td style="padding:15px">Get Bugzilla service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bugzilla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesBugzilla(id, body, callback)</td>
    <td style="padding:15px">Set bugzilla service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bugzilla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesBugzilla(id, callback)</td>
    <td style="padding:15px">Delete Atlassian Bamboo CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/bugzilla?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesBuildkite(id, callback)</td>
    <td style="padding:15px">Get Buildkite service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/buildkite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesBuildkite(id, body, callback)</td>
    <td style="padding:15px">Set buildkite service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/buildkite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesBuildkite(id, callback)</td>
    <td style="padding:15px">Delete Buildkite service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/buildkite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesBuildsEmail(id, body, callback)</td>
    <td style="padding:15px">Set builds-email service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/builds-email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesCampfire(id, callback)</td>
    <td style="padding:15px">Get Campfire service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/campfire?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesCampfire(id, body, callback)</td>
    <td style="padding:15px">Set campfire service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/campfire?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesCampfire(id, callback)</td>
    <td style="padding:15px">Delete Campfire service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/campfire?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesCustomIssueTracker(id, callback)</td>
    <td style="padding:15px">Get Custom Issue Tracker service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/custom-issue-tracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesCustomIssueTracker(id, body, callback)</td>
    <td style="padding:15px">Set custom-issue-tracker service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/custom-issue-tracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsServicesCustomIssueTracker(id, callback)</td>
    <td style="padding:15px">Delete Drone CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/custom-issue-tracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesDroneCi(id, callback)</td>
    <td style="padding:15px">Get Drone CI service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/drone-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesDroneCi(id, body, callback)</td>
    <td style="padding:15px">Set drone-ci service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/drone-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesDroneCi(id, callback)</td>
    <td style="padding:15px">Delete Drone CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/drone-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesEmailOnPush(id, callback)</td>
    <td style="padding:15px">Get Emails on push service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/emails-on-push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesEmailsOnPush(id, body, callback)</td>
    <td style="padding:15px">Set emails-on-push service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/emails-on-push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesEmailOnPush(id, callback)</td>
    <td style="padding:15px">Delete Emails on push service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/emails-on-push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesExternalWiki(id, callback)</td>
    <td style="padding:15px">Get External Wiki service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/external-wiki?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesExternalWiki(id, body, callback)</td>
    <td style="padding:15px">Set external-wiki service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/external-wiki?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesExternalWiki(id, callback)</td>
    <td style="padding:15px">Delete External Wiki service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/external-wiki?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesFlowdock(id, callback)</td>
    <td style="padding:15px">Get Flowdock service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/flowdock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesFlowdock(id, body, callback)</td>
    <td style="padding:15px">Set flowdock service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/flowdock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsServicesFlowdock(id, callback)</td>
    <td style="padding:15px">Delete External Wiki service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/flowdock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesHipchat(id, callback)</td>
    <td style="padding:15px">Get HipChat service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hipchat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesHipchat(id, body, callback)</td>
    <td style="padding:15px">Set hipchat service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hipchat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesHipchat(id, callback)</td>
    <td style="padding:15px">Delete HipChat service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hipchat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesIrker(id, callback)</td>
    <td style="padding:15px">Get Irker (IRC gateway) service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/irker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesIrker(id, body, callback)</td>
    <td style="padding:15px">Set irker service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/irker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesIrker(id, callback)</td>
    <td style="padding:15px">Delete Irker (IRC gateway) service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/irker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesJira(id, callback)</td>
    <td style="padding:15px">Get JIRA service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jira?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesJira(id, body, callback)</td>
    <td style="padding:15px">Set jira service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jira?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesJira(id, callback)</td>
    <td style="padding:15px">Remove all previously JIRA settings from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jira?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesKubernetes(id, callback)</td>
    <td style="padding:15px">Get Kubernetes service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/kubernetes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesKubernetes(id, body, callback)</td>
    <td style="padding:15px">Set kubernetes service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/kubernetes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesKubernetes(id, callback)</td>
    <td style="padding:15px">Delete Kubernetes service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/kubernetes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesMattermost(id, callback)</td>
    <td style="padding:15px">Get Mattermost notifications service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesMattermost(id, body, callback)</td>
    <td style="padding:15px">Set mattermost service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesMattermost(id, callback)</td>
    <td style="padding:15px">Delete Mattermost Notifications service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesMattermostSlashCommands(id, callback)</td>
    <td style="padding:15px">Get Mattermost slash command service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesMattermostSlashCommands(id, body, callback)</td>
    <td style="padding:15px">Set mattermost-slash-commands service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesMattermostSlashCommands(id, callback)</td>
    <td style="padding:15px">Delete Mattermost slash command service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mattermost-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesPipelinesEmail(id, callback)</td>
    <td style="padding:15px">Get Pipeline-Emails service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pipelines-email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesPipelinesEmail(id, body, callback)</td>
    <td style="padding:15px">Set pipelines-email service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pipelines-email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesPipelinesEmail(id, callback)</td>
    <td style="padding:15px">Delete Pipeline-Emails service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pipelines-email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesPivotaltracker(id, callback)</td>
    <td style="padding:15px">Get PivotalTracker service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pivotaltracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesPivotaltracker(id, body, callback)</td>
    <td style="padding:15px">Set pivotaltracker service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pivotaltracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesPivotaltracker(id, callback)</td>
    <td style="padding:15px">Delete PivotalTracker service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pivotaltracker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesPushover(id, callback)</td>
    <td style="padding:15px">Get Pushover service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pushover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesPushover(id, body, callback)</td>
    <td style="padding:15px">Set pushover service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pushover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesPushover(id, callback)</td>
    <td style="padding:15px">Delete Pushover service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/pushover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesRedmine(id, callback)</td>
    <td style="padding:15px">Get Redmine service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/redmine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesRedmine(id, body, callback)</td>
    <td style="padding:15px">Set redmine service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/redmine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesRedmine(id, callback)</td>
    <td style="padding:15px">Delete Redmine service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/redmine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesSlack(id, callback)</td>
    <td style="padding:15px">Get Slack service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesSlack(id, body, callback)</td>
    <td style="padding:15px">Set slack service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesSlack(id, callback)</td>
    <td style="padding:15px">Delete Slack service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesSlackSlashCommands(id, callback)</td>
    <td style="padding:15px">Get Slack slash command service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesSlackSlashCommands(id, body, callback)</td>
    <td style="padding:15px">Set slack-slash-commands service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesSlackSlashCommands(id, callback)</td>
    <td style="padding:15px">Delete Slack slash command service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/slack-slash-commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesTeamcity(id, callback)</td>
    <td style="padding:15px">Get JetBrains TeamCity CI service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/teamcity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesTeamcity(id, body, callback)</td>
    <td style="padding:15px">Set teamcity service for project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/teamcity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesTeamcity(id, callback)</td>
    <td style="padding:15px">Delete JetBrains TeamCity CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/teamcity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdShare(id, body, callback)</td>
    <td style="padding:15px">Share the project with a group</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/share?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdShareGroupId(id, groupId, callback)</td>
    <td style="padding:15px">deleteV4ProjectsIdShareGroupId</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/share/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippets(id, callback)</td>
    <td style="padding:15px">Get all project snippets</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippets(id, body, callback)</td>
    <td style="padding:15px">Create a new project snippet</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetId(id, snippetId, callback)</td>
    <td style="padding:15px">Get a single project snippet</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdSnippetsSnippetId(id, snippetId, body, callback)</td>
    <td style="padding:15px">Update an existing project snippet</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdSnippetsSnippetId(id, snippetId, callback)</td>
    <td style="padding:15px">Delete a project snippet</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetIdAwardEmoji(id, snippetId, page, perPage, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippetsSnippetIdAwardEmoji(id, snippetId, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(awardId, id, snippetId, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(awardId, id, snippetId, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji(id, snippetId, noteId, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji(id, snippetId, noteId, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(awardId, id, snippetId, noteId, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(awardId, id, snippetId, noteId, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsSnippetIdRaw(id, snippetId, callback)</td>
    <td style="padding:15px">Get a raw project snippet</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdStar(id, callback)</td>
    <td style="padding:15px">Star a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/star?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdStatusesSha(id, sha, body, callback)</td>
    <td style="padding:15px">Post status to a commit</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/statuses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdTriggers(id, callback)</td>
    <td style="padding:15px">Get triggers list</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdTriggers(id, callback)</td>
    <td style="padding:15px">Create a trigger</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdUnarchive(id, callback)</td>
    <td style="padding:15px">Unarchive a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdUploads(id, file, callback)</td>
    <td style="padding:15px">Upload a file</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/uploads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdUsers(id, search, callback)</td>
    <td style="padding:15px">Get the users list of a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdVariables(id, callback)</td>
    <td style="padding:15px">Get project variables</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdVariables(id, body, callback)</td>
    <td style="padding:15px">Create a new variable in a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdVariablesKey(id, key, callback)</td>
    <td style="padding:15px">Get a specific variable from a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdVariablesKey(id, key, body, callback)</td>
    <td style="padding:15px">Update an existing variable from a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdVariablesKey(id, key, callback)</td>
    <td style="padding:15px">Delete an existing variable from a project</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdForks(archived, visibility, orderBy, sort, search, simple, owned, membership, starred, statistics, withCustomAttributes, withIssuesEnabled, withMergeRequestsEnabled, minAccessLevel, id, callback)</td>
    <td style="padding:15px">List the projects accessible to the calling user that have a forked relationship with the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdUnstar(id, callback)</td>
    <td style="padding:15px">Unstars a given project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/unstar?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdLanguages(id, callback)</td>
    <td style="padding:15px">Get languages used in a project with percentage value.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/languages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPushRule(id, callback)</td>
    <td style="padding:15px">Get the push rules of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/push_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdPushRule(id, body, callback)</td>
    <td style="padding:15px">Edits a push rule for a specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/push_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4PorjectsIdPushRule(id, body, callback)</td>
    <td style="padding:15px">Adds a push rule to a specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/push_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delelteV4ProjectsIdPushRule(id, callback)</td>
    <td style="padding:15px">Removes a push rule from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/push_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdExport(id, callback)</td>
    <td style="padding:15px">Get the status of export.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdExport(id, body, callback)</td>
    <td style="padding:15px">Start a new export.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdExportDownload(id, callback)</td>
    <td style="padding:15px">Download the finished export.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/export/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdImport(file, pathParam, callback)</td>
    <td style="padding:15px">Import a file.</td>
    <td style="padding:15px">{base_path}/{version}/projects/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdImport(id, callback)</td>
    <td style="padding:15px">Get the status of an import.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/import/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBadges(id, callback)</td>
    <td style="padding:15px">Gets a list of a project’s badges and its group badges.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdBadges(id, linkUrl, imageUrl, callback)</td>
    <td style="padding:15px">Adds a badge to a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBadgesBadgeId(id, badgeId, callback)</td>
    <td style="padding:15px">Gets a badge of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdBadgesBadgeId(id, badgeId, body, callback)</td>
    <td style="padding:15px">Updates a badge of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdBadgesBadgeId(id, badgeId, callback)</td>
    <td style="padding:15px">Removes a badge from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdBadgesRender(id, imageUrl, linkUrl, callback)</td>
    <td style="padding:15px">Preview a badge from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/badges/render?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRegistryRepositories(id, callback)</td>
    <td style="padding:15px">Get a list of registry repositories in a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRegistryRepositoriesRepositoryId(id, repositoryId, callback)</td>
    <td style="padding:15px">Delete a repository in registry.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRegistryRepositoriesRepositoryIdTags(id, repositoryId, callback)</td>
    <td style="padding:15px">Get a list of tags for given registry repository.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags(id, repositoryId, callback)</td>
    <td style="padding:15px">Delete repository tags in bulk based on given criteria.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName(tagName, id, repositoryId, callback)</td>
    <td style="padding:15px">Get details of a registry repository tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName(tagName, id, repositoryId, callback)</td>
    <td style="padding:15px">Delete a registry repository tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/registry/repositories/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdCustomAttributes(id, callback)</td>
    <td style="padding:15px">Get all custom attributes on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/custom_attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdCustomAttributesKey(id, key, callback)</td>
    <td style="padding:15px">Get a single custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdCustomAttributesKey(id, key, body, callback)</td>
    <td style="padding:15px">Set a custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdCustomAttributesKey(id, key, callback)</td>
    <td style="padding:15px">Delete a custom attribute on a resource.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/custom_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobs(id, scope, callback)</td>
    <td style="padding:15px">Get a list of jobs in a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPipelinesPipelineIdJobs(id, pipelineId, scope, callback)</td>
    <td style="padding:15px">Get a list of jobs for a pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipelines/{pathv2}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobsJobId(id, jobId, callback)</td>
    <td style="padding:15px">Get a single job of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobsJobIdTrace(id, jobId, callback)</td>
    <td style="padding:15px">Get a trace of a specific job of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/trace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdJobsJobIdRetry(id, jobId, callback)</td>
    <td style="padding:15px">Retry a single job of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdJobsJobIdErase(id, jobId, callback)</td>
    <td style="padding:15px">Erase a single job of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/erase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdJobsJobIdCancel(id, jobId, callback)</td>
    <td style="padding:15px">Cancel a single job of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdJobsJobIdPlay(id, jobId, callback)</td>
    <td style="padding:15px">Triggers a manual action to start a job.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/play?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPagesDomains(id, callback)</td>
    <td style="padding:15px">Get a list of project pages domains.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pages/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPagesDomains(id, body, callback)</td>
    <td style="padding:15px">Creates a new pages domain.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pages/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPagesDomainsDomain(id, domain, callback)</td>
    <td style="padding:15px">Get a single project pages domain.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pages/domains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdPagesDomainsDomain(id, domain, body, callback)</td>
    <td style="padding:15px">Updates an existing project pages domain.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pages/domains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdPagesDomainsDomain(id, domain, callback)</td>
    <td style="padding:15px">Deletes an existing project pages domain.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pages/domains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPipelineSchedules(scope, id, callback)</td>
    <td style="padding:15px">Get a list of the pipeline schedules of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsPipelineSchedules(id, ref, description, cron, cronTimezone, active, callback)</td>
    <td style="padding:15px">Create a new pipeline schedule of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdPipelineSchedulesPipelineScheduleId(scope, id, pipelineScheduleId, callback)</td>
    <td style="padding:15px">Get a list of the pipeline schedules of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsPipelineSchedulesPipelineScheduleId(id, pipelineScheduleId, ref, description, cron, cronTimezone, active, callback)</td>
    <td style="padding:15px">Updates the pipeline schedule of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId(id, pipelineScheduleId, callback)</td>
    <td style="padding:15px">Delete the pipeline schedule of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership(id, pipelineScheduleId, callback)</td>
    <td style="padding:15px">Update the owner of the pipeline schedule of a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}/take_ownership?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables(id, pipelineScheduleId, value, variableType, callback)</td>
    <td style="padding:15px">Create a new variable of a pipeline schedule.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}/variables?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey(key, id, pipelineScheduleId, value, variableType, callback)</td>
    <td style="padding:15px">Updates the variable of a pipeline schedule.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey(id, pipelineScheduleId, key, callback)</td>
    <td style="padding:15px">Delete the variable of a pipeline schedule.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/pipeline_schedules/{pathv2}/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdClusters(id, callback)</td>
    <td style="padding:15px">Returns a list of project clusters.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdClustersClusterId(id, clusterId, callback)</td>
    <td style="padding:15px">Gets a single project cluster.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/clusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdClustesClusterId(id, clusterId, body, callback)</td>
    <td style="padding:15px">Updates an existing project cluster.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/clusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdClustersClusterId(id, clusterId, callback)</td>
    <td style="padding:15px">Deletes an existing project cluster.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/clusters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdClustersUser(id, body, callback)</td>
    <td style="padding:15px">Adds an existing Kubernetes cluster to the project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/clusters/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdProtectedBranches(id, callback)</td>
    <td style="padding:15px">Gets a list of protected branches from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProtectsIdProtectedBranches(name, pushAccessLevel, mergeAccessLevel, unprotectAccessLevel, allowedToPush, allowedToMerge, allowedToUnprotect, id, callback)</td>
    <td style="padding:15px">Protects a single repository branch or several project repository branches using a wildcard protected branch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdProtectedBranchesName(id, name, callback)</td>
    <td style="padding:15px">Gets a single protected branch or wildcard protected branch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_branches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdProtectedBranchesName(id, name, callback)</td>
    <td style="padding:15px">Unprotects the given protected branch or wildcard protected branch.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_branches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdProtectedTags(id, callback)</td>
    <td style="padding:15px">Gets a list of protected tags from a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdProtectedTags(id, name, createAccessLevel, callback)</td>
    <td style="padding:15px">Protects a single repository tag or several project repository tags using a wildcard protected tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdProtectedTagsName(name, id, callback)</td>
    <td style="padding:15px">Gets a single protected tag or wildcard protected tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdProtectedTagsName(id, name, callback)</td>
    <td style="padding:15px">Unprotects the given protected tag or wildcard protected tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/protected_tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdReleases(id, callback)</td>
    <td style="padding:15px">Paginated list of Releases, sorted by created_at.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/releases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdReleases(id, body, callback)</td>
    <td style="padding:15px">Create a Release.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/releases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdReleases(id, callback)</td>
    <td style="padding:15px">Delete a Release.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/releases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdReleasesTagName(id, tagName, callback)</td>
    <td style="padding:15px">Get a Release for the given tag.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/releases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdReleasesTagName(id, tagName, body, callback)</td>
    <td style="padding:15px">Update a Release.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/releases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdRepositorySubmodulesSubmodule(id, submodule, body, callback)</td>
    <td style="padding:15px">Update existing submodule reference in repository.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/submodules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueiidResourceLabelEvents(id, issueIid, callback)</td>
    <td style="padding:15px">Gets a list of all label events for a single issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/resource_label_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueiidResourceLabelEventsId(resourceLabelEventsId, issueIid, id, callback)</td>
    <td style="padding:15px">Returns a single label event for a specific project issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/resource_label_events/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestiidResourceLabelEvents(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Gets a list of all label events for a single merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/resource_label_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSearch(id, scope, search, callback)</td>
    <td style="padding:15px">Search within the specified project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdWikis(id, withContent, callback)</td>
    <td style="padding:15px">Get all wiki pages for a given project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdWikis(id, body, callback)</td>
    <td style="padding:15px">Creates a new wiki page for the given repository with the given title, slug, and content.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdWikisSlug(id, slug, callback)</td>
    <td style="padding:15px">Get a wiki page for a given project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdWikisSlug(id, slug, body, callback)</td>
    <td style="padding:15px">Updates an existing wiki page.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdWikis(id, slug, callback)</td>
    <td style="padding:15px">Deletes a wiki page with a given slug.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdWikisAttachments(withContent, id, file, branch, callback)</td>
    <td style="padding:15px">Uploads a file to the attachment folder inside the wiki’s repository.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/wikis/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsEnvironmentsEnvironmentIdStop(id, environmentId, callback)</td>
    <td style="padding:15px">Stop an environment</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/environments/{pathv2}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryFiles(id, filePath, ref, callback)</td>
    <td style="padding:15px">Get a file from repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdRepositoryFiles(id, filePath, body, callback)</td>
    <td style="padding:15px">Update existing file in repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdRepositoryFiles(id, filePath, body, callback)</td>
    <td style="padding:15px">Create new file in repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdRepositoryFiles(id, filePath, body, callback)</td>
    <td style="padding:15px">Delete an existing file in repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/files/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsRepositoryFilesRaw(ref, id, filePath, callback)</td>
    <td style="padding:15px">Get raw file from repository</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/files/{pathv2}/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobssJobIdArtifacts(id, jobId, callback)</td>
    <td style="padding:15px">Download the artifacts file from job</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdJobsJobIdArtifacts(id, jobId, callback)</td>
    <td style="padding:15px">Delete artifacts of a job.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdJobsJobIdArtifactsKeep(id, jobId, callback)</td>
    <td style="padding:15px">Keep the artifacts to prevent them from being deleted</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/artifacts/keep?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobsArtifactsRefNameDownload(id, refName, job, callback)</td>
    <td style="padding:15px">Download the artifacts file from job</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/artifacts/{pathv2}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobsJobIdArtifactsArtifcactPath(id, jobId, artifactPath, callback)</td>
    <td style="padding:15px">Download a single artifact file from a job with a specified ID</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/{pathv2}/artifacts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdJobsArtifactsRefNameRaw(id, refName, job, artifactPath, callback)</td>
    <td style="padding:15px">Download a single artifact file from a specific tag or branch from within the job’s artifacts archive.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/jobs/artifacts/{pathv2}/raw/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdRepositoryCommitsShaRefs(id, sha, type, callback)</td>
    <td style="padding:15px">Get references a commit is pushed to.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/repository/commits/{pathv2}/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdTriggersTriggerId(id, triggerId, callback)</td>
    <td style="padding:15px">Get details of project’s build trigger.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdTriggersTriggerId(triggerId, id, body, callback)</td>
    <td style="padding:15px">Update a trigger for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdTriggersTriggerId(id, triggerId, callback)</td>
    <td style="padding:15px">Remove a project’s build trigger.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdTriggersTriggerIdOwnership(triggerId, id, body, callback)</td>
    <td style="padding:15px">Update an owner of a project trigger.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/triggers/{pathv2}/take_ownership?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesHangoutschat(id, callback)</td>
    <td style="padding:15px">Get Hangouts Chat service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hangouts-chat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesHangoutschat(id, body, callback)</td>
    <td style="padding:15px">Set Hangouts Chat service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hangouts-chat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesHangoutschat(id, callback)</td>
    <td style="padding:15px">Delete Hangouts Chat service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/hangouts-chat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesPackagist(id, callback)</td>
    <td style="padding:15px">Get Packagist service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/packagist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesPackagist(id, body, callback)</td>
    <td style="padding:15px">Set Packagist service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/packagist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesPackagist(id, callback)</td>
    <td style="padding:15px">Delete Packagist service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/packagist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesMicrosoftTeams(id, callback)</td>
    <td style="padding:15px">Get Microsoft Teams service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/microsoft-teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesMicrosoftTeams(id, body, callback)</td>
    <td style="padding:15px">Set Microsoft Teams service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/microsoft-teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesMicrosoftTeams(id, callback)</td>
    <td style="padding:15px">Delete Microsoft Teams service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/microsoft-teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesJenkins(id, callback)</td>
    <td style="padding:15px">Get Jenkins CI service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jenkins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesJenkins(id, body, callback)</td>
    <td style="padding:15px">Set Jenkins CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jenkins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesJenkins(id, callback)</td>
    <td style="padding:15px">Delete Jenkins CI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/jenkins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesMockCi(id, callback)</td>
    <td style="padding:15px">Get MockCI service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mock-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesMockCi(id, body, callback)</td>
    <td style="padding:15px">Set MockCI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mock-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesMockCi(id, callback)</td>
    <td style="padding:15px">Delete MockCI service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/mock-ci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdServicesYoutrack(id, callback)</td>
    <td style="padding:15px">Get YouTrack service settings for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/youtrack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdServicesYoutrack(id, body, callback)</td>
    <td style="padding:15px">Set YouTrack service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/youtrack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdServicesYoutrack(id, callback)</td>
    <td style="padding:15px">Delete YouTrack service for a project.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/services/youtrack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetIdDiscussions(snippetId, id, callback)</td>
    <td style="padding:15px">Gets a list of all discussions for a single snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippetIdDiscussions(bodyQuery, id, snippetId, body, callback)</td>
    <td style="padding:15px">Creates a new discussion to a single project snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetIdDiscussionId(id, snippetId, discussionId, callback)</td>
    <td style="padding:15px">Returns a single discussion for a specific project snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippetIdDiscussionIdNotes(bodyQuery, id, snippetId, discussionId, body, callback)</td>
    <td style="padding:15px">Adds a new note to the discussion.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions/{pathv3}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdSnippetIdDiscussionIdNoteId(id, snippetId, discussionId, noteId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Modify existing discussion note of a snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdSnippetIdDiscussionIdNoteId(id, snippetId, discussionId, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing discussion note of a snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIidDiscussions(id, issueIid, callback)</td>
    <td style="padding:15px">Gets a list of all discussions for a single issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIidDiscussions(bodyQuery, id, issueIid, body, callback)</td>
    <td style="padding:15px">Creates a new discussion to a single project issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIidDiscussionsId(id, issueIid, discussionId, callback)</td>
    <td style="padding:15px">Returns a single discussion for a specific project issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIidDiscussionsIdNotes(bodyQuery, id, issueIid, discussionId, body, callback)</td>
    <td style="padding:15px">Adds a new note to the discussion.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions/{pathv3}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdIssuesIidDiscussionsIdNotesId(bodyQuery, id, issueIid, discussionId, noteId, body, callback)</td>
    <td style="padding:15px">Modify existing discussion note of an issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId(id, issueIid, discussionId, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing discussion note of an issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsIidDiscussions(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Gets a list of all discussions for a single merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsIidDiscussionsId(id, mergeRequestIid, discussionId, callback)</td>
    <td style="padding:15px">Returns a single discussion for a specific project merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMergeRequestsIidDiscussionsId(resolved, id, mergeRequestIid, discussionId, body, callback)</td>
    <td style="padding:15px">Resolve/unresolve whole discussion of a merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId(bodyQuery, resolved, id, mergeRequestIid, discussionId, noteId, body, callback)</td>
    <td style="padding:15px">Modify or resolve an existing discussion note of a merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId(id, mergeRequestIid, discussionId, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing discussion note of a merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdCommitsIdDiscussions(id, commitId, callback)</td>
    <td style="padding:15px">Gets a list of all discussions for a single commit.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdCommitsIdDiscussions(bodyQuery, id, commitId, body, callback)</td>
    <td style="padding:15px">Creates a new discussion to a single project commit.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdCommitsIdDiscussionsId(id, commitId, discussionId, callback)</td>
    <td style="padding:15px">Returns a single discussion for a specific project commit</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdCommitsIdDiscussionsIdNotes(bodyQuery, id, commitId, discussionId, body, callback)</td>
    <td style="padding:15px">Adds a new note to the discussion.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions/{pathv3}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdCommitsIdDiscussionsIdNotesId(id, commitId, discussionId, noteId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Modify or resolve an existing discussion note of a commit.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId(id, commitId, discussionId, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing discussion note of a commit.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/commits/{pathv2}/discussions/{pathv3}/notes/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsIdNotes(id, snippetId, sort, orderBy, callback)</td>
    <td style="padding:15px">Gets a list of all notes for a single snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdSnippetsIdNotes(id, snippetId, bodyQuery, createdAt, body, callback)</td>
    <td style="padding:15px">Creates a new note for a single snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdSnippetsIdNoteId(id, noteId, snippetId, callback)</td>
    <td style="padding:15px">Returns a single note for a given snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdSnippetsIdNoteId(id, noteId, snippetId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Modify existing note of a snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdSnippetsIdNoteId(id, noteId, snippetId, callback)</td>
    <td style="padding:15px">Deletes an existing note of a snippet.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/snippets/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsIidNotes(id, mergeRequestIid, sort, orderBy, callback)</td>
    <td style="padding:15px">Gets a list of all notes for a single merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsIidNotes(id, mergeRequestIid, bodyQuery, body, callback)</td>
    <td style="padding:15px">Create a new +noteable+ note</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIdSubscribe(id, issueIid, body, callback)</td>
    <td style="padding:15px">Subscribes the authenticated user to an issue to receive notifications.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIdUnsubscribe(id, issueIid, body, callback)</td>
    <td style="padding:15px">Unsubscribes the authenticated user from the issue to not receive notifications from it.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/unsubscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdMove(id, issueIid, toProjectId, callback)</td>
    <td style="padding:15px">Move an existing issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIidNotes(id, issueIid, sort, orderBy, callback)</td>
    <td style="padding:15px">Gets a list of all notes for a single issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIidNotes(id, issueIid, bodyQuery, body, callback)</td>
    <td style="padding:15px">Creates a new note to a single project issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(id, noteId, issueIid, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(id, noteId, issueIid, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(awardId, id, noteId, issueIid, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(awardId, id, noteId, issueIid, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIidNotesId(id, issueIid, noteId, callback)</td>
    <td style="padding:15px">Modify existing note of an issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdIssuesIidNotesId(id, bodyQuery, issueIid, noteId, body, callback)</td>
    <td style="padding:15px">Modify existing note of an issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdIssuesIidNotesId(id, issueIid, noteId, callback)</td>
    <td style="padding:15px">Deletes an existing note of an issue.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueId(id, issueIid, callback)</td>
    <td style="padding:15px">Get a single project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdIssuesIssueId(id, issueIid, body, callback)</td>
    <td style="padding:15px">Update an existing issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdIssuesIssueId(id, issueIid, callback)</td>
    <td style="padding:15px">Delete a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdAddSpentTime(id, issueIid, body, callback)</td>
    <td style="padding:15px">Add spent time for a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/add_spent_time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueIdAwardEmoji(id, issueIid, page, perPage, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdAwardEmoji(id, issueIid, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(id, issueIid, awardId, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(id, issueIid, awardId, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdResetSpentTime(id, issueIid, callback)</td>
    <td style="padding:15px">Reset spent time for a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/reset_spent_time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdResetTimeEstimate(id, issueIid, callback)</td>
    <td style="padding:15px">Reset the time estimate for a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/reset_time_estimate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdTimeEstimate(id, issueIid, body, callback)</td>
    <td style="padding:15px">Set a time estimate for a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/time_estimate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdIssuesIssueIdTimeStats(id, issueIid, callback)</td>
    <td style="padding:15px">Show time stats for a project issue</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/time_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdIssuesIssueIdTodo(id, issueIid, callback)</td>
    <td style="padding:15px">Create a todo on an issuable</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/issues/{pathv2}/todo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdLabelsIdSubscribe(id, labelId, body, callback)</td>
    <td style="padding:15px">Subscribes the authenticated user to a label to receive notifications.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels/{pathv2}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdLabelsIdUnsubscribe(id, labelId, body, callback)</td>
    <td style="padding:15px">Unsubscribes the authenticated user from a label to not receive notifications from it.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/labels/{pathv2}/unsubscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestId(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get a single merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMergeRequestsMergeRequestId(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Update a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMergeRequestsMergeRequestId(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Delete a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Add spent time for a project merge_request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/add_spent_time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji(id, page, perPage, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(id, mergeRequestIid, awardId, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(id, mergeRequestIid, awardId, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/award_emoji/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Cancel merge if "Merge When Pipeline Succeeds" is enabled</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/cancel_merge_when_build_succeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdChanges(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Show the merge request changes</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues(id, page, perPage, mergeRequestIid, callback)</td>
    <td style="padding:15px">List issues that will be closed on merge</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/closes_issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdComments(id, page, perPage, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get the comments of a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdComments(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Post a comment to a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdCommits(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get the commits of a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMergeRequestsMergeRequestIdMerge(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Merge a merge request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji(page, perPage, id, mergeRequestIid, noteId, callback)</td>
    <td style="padding:15px">Get a list of project +awardable+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji(id, mergeRequestIid, noteId, body, callback)</td>
    <td style="padding:15px">Award a new Emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}/award_emoji?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId(id, mergeRequestIid, noteId, awardId, callback)</td>
    <td style="padding:15px">Get a specific award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId(id, mergeRequestIid, noteId, awardId, callback)</td>
    <td style="padding:15px">Delete a +awardables+ award emoji</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}/award_emoji/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Reset spent time for a project merge_request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/reset_spent_time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Reset the time estimate for a project merge_request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/reset_time_estimate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Set a time estimate for a project merge_request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/time_estimate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Show time stats for a project merge_request</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/time_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsMergeRequestIdTodo(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Create a todo on an issuable</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/todo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdVersions(id, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get a list of merge request diff versions</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId(id, versionId, mergeRequestIid, callback)</td>
    <td style="padding:15px">Get a single merge request diff version</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/versions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestsIidNotesId(id, mergeRequestIid, noteId, callback)</td>
    <td style="padding:15px">Get a single +noteable+ note</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4ProjectsIdMergeRequestsIidNotesId(id, mergeRequestIid, noteId, bodyQuery, body, callback)</td>
    <td style="padding:15px">Update an existing +noteable+ note</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ProjectsIdMergeRequestsIidNotesId(id, noteId, mergeRequestIid, callback)</td>
    <td style="padding:15px">Delete a +noteable+ note</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/notes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestIidDiscussionsIdNotes(bodyQuery, id, mergeRequestIid, discussionId, body, callback)</td>
    <td style="padding:15px">Adds a new note to the discussion.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/discussions/{pathv3}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ProjectsIdMergeRequestiidResourceLabelEventId(id, mergeRequestIid, resourceLabelEventId, callback)</td>
    <td style="padding:15px">Returns a single label event for a specific project merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/resource_label_events/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsIidSubscribe(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Subscribes the authenticated user to a merge request to receive notification.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ProjectsIdMergeRequestsIidUnsubscribe(id, mergeRequestIid, body, callback)</td>
    <td style="padding:15px">Unsubscribes the authenticated user from a merge request to not receive notifications from that merge request.</td>
    <td style="padding:15px">{base_path}/{version}/projects/{pathv1}/merge_requests/{pathv2}/unsubscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Runners(scope = 'active', status, tagList, callback)</td>
    <td style="padding:15px">Get runners available for user</td>
    <td style="padding:15px">{base_path}/{version}/runners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4RunnersAll(type, status, tagList, callback)</td>
    <td style="padding:15px">Get all runners - shared and specific</td>
    <td style="padding:15px">{base_path}/{version}/runners/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4RunnersId(id, callback)</td>
    <td style="padding:15px">Get runner's details</td>
    <td style="padding:15px">{base_path}/{version}/runners/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4RunnersId(id, body, callback)</td>
    <td style="padding:15px">Update runner's details</td>
    <td style="padding:15px">{base_path}/{version}/runners/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4RunnersId(id, callback)</td>
    <td style="padding:15px">Remove a runner</td>
    <td style="padding:15px">{base_path}/{version}/runners/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Session(body, callback)</td>
    <td style="padding:15px">Login to get token</td>
    <td style="padding:15px">{base_path}/{version}/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SidekiqCompoundMetrics(callback)</td>
    <td style="padding:15px">Get the Sidekiq Compound metrics. Includes queue, process, and job statistics</td>
    <td style="padding:15px">{base_path}/{version}/sidekiq/compound_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SidekiqJobStats(callback)</td>
    <td style="padding:15px">Get the Sidekiq job statistics</td>
    <td style="padding:15px">{base_path}/{version}/sidekiq/job_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SidekiqProcessMetrics(callback)</td>
    <td style="padding:15px">Get the Sidekiq process metrics</td>
    <td style="padding:15px">{base_path}/{version}/sidekiq/process_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SidekiqQueueMetrics(callback)</td>
    <td style="padding:15px">Get the Sidekiq queue metrics</td>
    <td style="padding:15px">{base_path}/{version}/sidekiq/queue_metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Snippets(callback)</td>
    <td style="padding:15px">Get a snippets list for authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Snippets(body, callback)</td>
    <td style="padding:15px">Create new snippet</td>
    <td style="padding:15px">{base_path}/{version}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SnippetsPublic(callback)</td>
    <td style="padding:15px">List all public snippets current_user has access to</td>
    <td style="padding:15px">{base_path}/{version}/snippets/public?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SnippetsId(id, callback)</td>
    <td style="padding:15px">Get a single snippet</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4SnippetsId(id, body, callback)</td>
    <td style="padding:15px">Update an existing snippet</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4SnippetsId(id, callback)</td>
    <td style="padding:15px">Remove snippet</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SnippetsIdRaw(id, callback)</td>
    <td style="padding:15px">Get a raw snippet</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesDockerfiles(callback)</td>
    <td style="padding:15px">Get the list of the available template</td>
    <td style="padding:15px">{base_path}/{version}/templates/dockerfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesGitignores(callback)</td>
    <td style="padding:15px">Get the list of the available template</td>
    <td style="padding:15px">{base_path}/{version}/templates/gitignores?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesGitlabCiYmls(callback)</td>
    <td style="padding:15px">Get the list of the available template</td>
    <td style="padding:15px">{base_path}/{version}/templates/gitlab_ci_ymls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesLicenses(popular, callback)</td>
    <td style="padding:15px">Get the list of the available license template</td>
    <td style="padding:15px">{base_path}/{version}/templates/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesDockerfilesKey(key, callback)</td>
    <td style="padding:15px">Get a single Dockerfile template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/dockerfiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesGitignoresKey(key, callback)</td>
    <td style="padding:15px">Get a single .gitignore template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/gitignores/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesGitlabCiYmlsKey(key, callback)</td>
    <td style="padding:15px">Get a single GitLab CI YML template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/gitlab_ci_ymls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TemplatesLicensesKey(project, fullname, key, callback)</td>
    <td style="padding:15px">Get a single license template.</td>
    <td style="padding:15px">{base_path}/{version}/templates/licenses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Todos(action, authorId, projectId, groupId, state, type, callback)</td>
    <td style="padding:15px">Get a todo list</td>
    <td style="padding:15px">{base_path}/{version}/todos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4TodosMarkAsDone(callback)</td>
    <td style="padding:15px">Marks all pending todos for the current user as done.</td>
    <td style="padding:15px">{base_path}/{version}/todos/mark_as_done?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4TodosId(id, callback)</td>
    <td style="padding:15px">Mark a todo as done</td>
    <td style="padding:15px">{base_path}/{version}/todos/{pathv1}/mark_as_done?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Users(orderBy, sort, twoFactor, callback)</td>
    <td style="padding:15px">Get the list of users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Users(body, callback)</td>
    <td style="padding:15px">Create a user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersId(id, callback)</td>
    <td style="padding:15px">Get a single user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4UsersId(id, body, callback)</td>
    <td style="padding:15px">Update a user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4UsersId(id, callback)</td>
    <td style="padding:15px">Delete a user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4UsersIdBlock(id, callback)</td>
    <td style="padding:15px">Block a user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersIdEmails(id, callback)</td>
    <td style="padding:15px">Get the emails addresses of a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4UsersIdEmails(id, body, callback)</td>
    <td style="padding:15px">Add an email address to a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4UsersIdEmailsEmailId(id, emailId, callback)</td>
    <td style="padding:15px">Delete an email address of a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/emails/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersIdEvents(id, page, perPage, callback)</td>
    <td style="padding:15px">Get the contribution events of a specified user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersIdKeys(id, callback)</td>
    <td style="padding:15px">Get the SSH keys of a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4UsersIdKeys(id, body, callback)</td>
    <td style="padding:15px">Add an SSH key to a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4UsersIdKeysKeyId(id, keyId, callback)</td>
    <td style="padding:15px">Delete an existing SSH key from a specified user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4UsersIdUnblock(id, callback)</td>
    <td style="padding:15px">Unblock a user. Available only for admins.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/unblock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersIdProjects(id, callback)</td>
    <td style="padding:15px">Get a list of visible projects owned by the given user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsersIdStatus(id, callback)</td>
    <td style="padding:15px">Get the status of a user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Version(callback)</td>
    <td style="padding:15px">Retrieve version information for this GitLab instance.</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Applications(callback)</td>
    <td style="padding:15px">List all registered applications</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Applications(callback)</td>
    <td style="padding:15px">Create an application by posting a JSON payload</td>
    <td style="padding:15px">{base_path}/{version}/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4Application(id, callback)</td>
    <td style="padding:15px">Delete a specific application</td>
    <td style="padding:15px">{base_path}/{version}/applications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4PagesDomains(callback)</td>
    <td style="padding:15px">Get a list of all pages domains.</td>
    <td style="padding:15px">{base_path}/{version}/pages/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Avatar(email, size, callback)</td>
    <td style="padding:15px">Get a single avatar URL for a user with the given email address.</td>
    <td style="padding:15px">{base_path}/{version}/avatar?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4BroadcastMessage(callback)</td>
    <td style="padding:15px">List all broadcast messages.</td>
    <td style="padding:15px">{base_path}/{version}/broadcast_messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4BroadcastMessage(body, callback)</td>
    <td style="padding:15px">Create a new broadcast message.</td>
    <td style="padding:15px">{base_path}/{version}/broadcast_messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4BroadcastMessageId(id, callback)</td>
    <td style="padding:15px">Get a specific broadcast message.</td>
    <td style="padding:15px">{base_path}/{version}/broadcast_messages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4BroadcastMessageId(id, body, callback)</td>
    <td style="padding:15px">Update an existing broadcast message.</td>
    <td style="padding:15px">{base_path}/{version}/broadcast_messages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4BroadcastMessageId(id, callback)</td>
    <td style="padding:15px">Delete a broadcast message.</td>
    <td style="padding:15px">{base_path}/{version}/broadcast_messages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Events(callback)</td>
    <td style="padding:15px">Get a list of events for the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Features(callback)</td>
    <td style="padding:15px">Get a list of all persisted features, with its gate values.</td>
    <td style="padding:15px">{base_path}/{version}/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4FeaturesName(name, body, callback)</td>
    <td style="padding:15px">Set a feature’s gate value.</td>
    <td style="padding:15px">{base_path}/{version}/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4FeaturesName(name, callback)</td>
    <td style="padding:15px">Removes a feature gate.</td>
    <td style="padding:15px">{base_path}/{version}/features/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4ImportGithub(body, callback)</td>
    <td style="padding:15px">Import your projects from GitHub to GitLab via the API.</td>
    <td style="padding:15px">{base_path}/{version}/import/github?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Markdown(body, callback)</td>
    <td style="padding:15px">Render an arbitrary Markdown document.</td>
    <td style="padding:15px">{base_path}/{version}/markdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4MergeRequests(state, orderBy, sort, milestone, view, labels, createdAfter, updatedAfter, scope, authorId, assigneeId, approverIds, search, sourceBranch, targetBrach, wip, inParam, myReactionEmoji, createdBefore, updatedBefore, callback)</td>
    <td style="padding:15px">Get all merge requests the authenticated user has access to.</td>
    <td style="padding:15px">{base_path}/{version}/merge_requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Search(scope, search, callback)</td>
    <td style="padding:15px">Search globally across the GitLab instance.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4SuggestionIdApply(id, callback)</td>
    <td style="padding:15px">Applies a suggested patch in a merge request.</td>
    <td style="padding:15px">{base_path}/{version}/suggestions/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Lint(body, callback)</td>
    <td style="padding:15px">Validation of .gitlab-ci.yml content</td>
    <td style="padding:15px">{base_path}/{version}/lint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4SubGroups(id, callback)</td>
    <td style="padding:15px">Get all subgroups for a single group.</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/subgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
