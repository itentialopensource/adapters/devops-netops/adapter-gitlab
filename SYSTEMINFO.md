# GitLab

Vendor: GitLab
Homepage: https://about.gitlab.com/

Product: GitLab
Product Page: https://about.gitlab.com/

## Introduction
We classify GitLab into the CI/CD domain as GitLab handles the versioning, building, storage and deploying of the system and components.

## Why Integrate
The GitLab adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GitLab. With this adapter you have the ability to perform operations such as:

- Get a GitLab project's information
- Commit a new change to a GitLab repository 
- Create or manage a merge request in a GitLab repository

## Additional Product Documentation
The [API documents for Gitlab](https://docs.gitlab.com/ee/api/rest/)